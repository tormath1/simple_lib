from ..simple_lib import add


def test_simple_add():

    assert add(5, 7) == 12


def test_add_with_zero():

    assert add(4, 0) == 4


def test_add_with_two_digits():

    assert add(12, 45) == 57
